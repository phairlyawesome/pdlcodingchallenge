""" WELCOME TO THE PDL CODING CHALLENGE!! """

'''
STEP 1:

Read both data files: transaction_information.csv and customer_preferences.json

transaction_information.csv is tab delimited and customer_preferences.json is record oriented
'''

'''
STEP 2: 

Merge the dataframes in to one master dataframe

There are 500 customers in the transaction dataframe and 300 customers in the customer preference dataframe
Only keep records which appear in both

remove the objects in memory that are no longer needed
'''

''' 
STEP 3:

Calculate total sales by month

Manipulate the data with a single chained pandas function
'''

'''
STEP 4:

Use any model of your choice to understand the relationship between a person's sales and other metrics

Make sure to use categorical and continuous variables
'''

'''
STEP 5:

Create a custom object called Customer which is initialized with a customer's id, total transaction count, and total 
sales. 

This object should contain a method called average_transaction_size() which computes and returns the average 
transaction size for the customer.  

Initialize a Customer object for customer 1 and print its average transaction size.
'''
